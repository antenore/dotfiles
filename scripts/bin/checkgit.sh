#!/bin/sh

START_DATE="July 1, 2017"
END_DATE="July 1, 2018"

git remote update
git checkout origin/master

repo="`git remote show origin | grep "Fetch URL" | awk '{ print $3 }'`"

author=`git log --author "$1" --no-merges --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$author" -ne 0
then
	msg="$msg Authored $author"
fi

committer=`git log --no-merges --no-merges --committer "$1" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$committer" -ne 0
then
	msg="$msg Committer $committer"
fi

mergesa=`git log --merges --author "$1" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
mergesc=`git log --merges --committer "$1" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
merges=`expr $mergesa + $mergesc`
if test "$merges" -ne 0
then
	msg="$msg Merges $merges"
fi

reviewer=`git log --grep "^ *Reviewed.* *$1>" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$reviewer" -ne 0
then
	msg="$msg Reviewer $reviewer"
fi

ack=`git log --grep "^ *Acked.* *$1>" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$ack" -ne 0
then
	msg="$msg Ack $ack"
fi

test=`git log --grep "^ *Tested.* *$1>" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$test" -ne 0
then
	msg="$msg Tester $test"
fi

suggest=`git log --grep "^ *Suggested.* *$1>" --pretty=oneline --since "$START_DATE" --until "$END_DATE" | wc -l`
if test "$suggest" -ne 0
then
	msg="$msg Suggester $suggest"
fi
echo $repo $1 $msg
