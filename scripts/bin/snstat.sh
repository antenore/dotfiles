#!/bin/bash -
#===============================================================================
#
#          FILE: snstat.sh
#
#         USAGE: ./snstat.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (),
#  ORGANIZATION:
#       CREATED: 02/28/2018 03:08:01 PM
#      REVISION:  ---
#===============================================================================

LOGFILE="/var/opt/log/prov/snstat.log"
BCK_LOGFILE="$LOGFILE.$(date +%Y%m%d%H%M)"
if [[ ! -f "$LOGFILE" ]] ; then
    touch "$LOGFILE"
else
    # 2 execution in the same minute looks weird
    if [[ ! -f "$BCK_LOGFILE" ]] ; then
        mv "$LOGFILE" "$LOGFILE.$(date +%Y%m%d%H%M)"
    fi
fi
case "$(uname -s)" in

    AIX)
        PLATFORM="AIX"
        netstat -anf inet | grep tcp | \
            awk -v date="$(date +%Y%m%d%H%M)" '{print date "\t" $4 "\t" $5 "\t" $6}' > "$LOGFILE"
        ;;
    Linux)
        PLATFORM="Linux"
        netstat -an4t | \
            awk -v date="$(date +%Y%m%d%H%M)" '{print date "\t" $4 "\t" $5 "\t" $6}' > "$LOGFILE"
        ;;
    SunOS)
        PLATFORM="SunOS"
        netstat -an -f inet -P tcp | \
            awk -v date="$(date +%Y%m%d%H%M)" '{print date "\t" $1 "\t" $2 "\t" $NF}' > "$LOGFILE"
        ;;
esac

if [[ "$(awk -F'\t' '{print NF; exit}' "$LOGFILE")" -ne 4 ]] ; then
    echo "$(hostname ERROR)" > "$LOGFILE"
fi




