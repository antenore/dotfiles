#!/bin/bash -

# --------------------------------------------------------------------------
#
# Public Script: remote_bsni.sh
#
# Title:
#
# Category:
#    Public interface
#
# Synopsis:
# >     ./remote_bsni.sh
#
# Description:
#
# Usage:
# >     ./remote_bsni.sh
#
# where:
# >         -h  Help
#
# Examples:
# >   ./remote_bsni.sh
#
# Exit Codes:
#   0  - OK
#
# Author:
#   Antenore Gatta (agat), agat@ch.ibm.com
#
# Copyright: IBM Switzerland
#
# Created:
#   03/14/2018 11:21:45 AM
#

#==== REVISION LOG =============================================================
#    DATE     |   TAG    |   WHO      |   VERSION    |   DESCRIPTION
#===============================================================================
# 03/14/2018  |   N/A    | agat       |   1.0 18W    | Creation
#==== END REVISION LOG==========================================================

while IFS= read -r -u9 host ; do
    printf "****************\n     $host\n****************\n"
    ssh -qtt -i ~/.ssh/id_rsa_4096_z007093 ch027118@sibwi332 ssh -qtt chu07159@${host} "wget --spider --no-check-certificate -t 1 -T 3 https://9.143.26.137/"
done 9< <(awk -F\; '/BSNI/{print $1}' ~/APAR/Unix_ServerList.csv.20180314 | grep -v ARCHIVED)


