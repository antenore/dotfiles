#!/bin/bash -
#===============================================================================
#
#          FILE: rem_testargs.sh
#
#         USAGE: ./rem_testargs.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (),
#  ORGANIZATION:
#       CREATED: 02/27/2018 04:01:23 PM
#      REVISION:  ---
#===============================================================================

c=1
 for i in "$@" ; do
	 echo "Arg $c of $# is $i"
	 c=$((c+1))
 done
