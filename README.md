dotfiles
========

![Screenshot](data/i3wm-reddit.png)

* WM: [i3-gaps](https://www.github.com/Airblader/i3) + [i3 GNOME integration for 3.28.1](https://github.com/i3-gnome/i3-gnome/tree/3.28.1)
* Compositor: [yshui/picom](https://github.com/yshui/picom)
* Bar: [Polybar](https://github.com/polybar/polybar)
* Wallpaper: https://pxhere.com/en/photo/664086
* Colors: Almost everything generated with [Pywal](https://github.com/dylanaraps/pywal) and heavily modified.
* Icons: a modified version of Papirus, to match the color scheme.
* Fonts: [be5invis/Iosevka.](https://github.com/be5invis/iosevka), [nerd-fonts/Inconsolata](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Inconsolata/complete), [Feather icons](https://feathericons.com/)
* ranger, zsh, vim/neovim, st, tmux and tons of other things, check-out my dotfiles.

## Description

I keep most of my dotfiles in sync with git on github. After I've tried different approaches,
I came across GNU stow, that makes things easier.

All the dotfiles and some of my scripts are organized inside folders that acts like packages.
Executing stow from the dotfile folder, it will create one link in the parent directory foreach
file, or folder, found inside the specified "package".

## Installation

- Install [GNU Stow](http://www.gnu.org/software/stow/)

        i.e. sudo pacman -S stow

- Clone to ~/.dotfiles, and enter the directory.

        cd ~/.dotfiles

- Symlink the .file into place with stow (if the dotfiles folder is in the home directory).

        stow --ignore ".xsession" X colors conky init \
                                  scripts spectrwm    \
                                  top urxvt vim zsh          # I don't use .xession at the moment

## Colors

Colors have been generated with pywal using the `colorz` back-end

```
wal -i ~/Pictures/wallpapers/sand-car.png --backend colorz
```

wal will generate all the colors under `~/.cache/wal` and apply most of the colors by itself.

I've used Oomox theme designer to edit the Gtk/GNOME theme using the file `colors-oomox` and change some of the colors to my pleasure and needs.

I've also modified most of the generated files under the ~/.cache directory and run `wal -R` to apply the new colors.

Beside this I had to force pywal and add other customisazions.

KDE is still something I've to take care about, as wal doesn't generate anything for Plasma.

+ **Slack**: #230F05,#6A1B1B,#B24E1E,#E6E6FA,#B24E1E,#E6E6FA,#D67739,#D67739,#230F05,#E6E6FA

An example with [dpayne/cli-visualizer: CLI based audio visualizer](https://github.com/dpayne/cli-visualizer)

[![asciicast](https://asciinema.org/a/iH4cMcZqNJcZ1bP7CgNiXIxEN.svg)](https://asciinema.org/a/iH4cMcZqNJcZ1bP7CgNiXIxEN)

## TODO

- Write a Makefile

